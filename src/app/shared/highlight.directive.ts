import {
  Directive,
  ElementRef,
  Input,
  OnInit,
  HostBinding,
  HostListener
} from "@angular/core";

@Directive({
  selector: "[appHighlight]"
  // host:{
  //   '[style.color]':'appHighlight',
  //   '(mouseenter)':'activate($event)'
  // }
})
export class HighlightDirective implements OnInit {
  
  @Input()
  appHighlight;
  
  @HostBinding("style.color")
  currentColor

  hover = false;

  @HostListener("mouseenter", ["$event"])
  activate(event) {
    this.currentColor = this.appHighlight
  }
  
  @HostListener("mouseleave")
  deactivate() {
    this.currentColor = ''
  }

  constructor(private elem: ElementRef) {}

  ngOnInit() {
    // console.log("HEllo appHighlight", this.elem);

    // this.elem.nativeElement.style.color = this.appHighlight;
  }
}

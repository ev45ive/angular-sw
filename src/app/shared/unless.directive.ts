import {
  Directive,
  TemplateRef,
  ViewContainerRef,
  ComponentFactoryResolver,
  Input,
  OnChanges
} from "@angular/core";
import { PlaylistsViewComponent } from "../playlists/views/playlists-view/playlists-view.component";

type Context = {};

@Directive({
  selector: "[appUnless]"
})
export class UnlessDirective {
  @Input()
  appUnless;

  ngOnChanges() {
    if (this.appUnless) {
      this.vcr.clear();
    } else {
      this.vcr.createEmbeddedView(this.tpl, {}, 0);
    }
  }

  constructor(
    protected tpl: TemplateRef<Context>,
    protected vcr: ViewContainerRef,
    protected cfr: ComponentFactoryResolver
  ) {
    console.log("Hello!", tpl, vcr);
  }
}

// const f = this.cfr.resolveComponentFactory(PlaylistsViewComponent);

// const c = this.vcr.createComponent(f);
// c.instance.playlists;

import { UnlessDirective } from "./unless.directive";
import {
  TemplateRef,
  ViewContainerRef,
  ComponentFactoryResolver
} from "@angular/core";

describe("UnlessDirective", () => {
  it("should create an instance", () => {

    const directive = new UnlessDirective(
      {} as any,
      {} as any,
      {} as any,
    );
    expect(directive).toBeTruthy();
  });
});

import { Injectable, Inject, EventEmitter } from "@angular/core";
import { Album } from "src/app/models/Album";
import { API_SEARCH_URL } from "./tokens";
import { HttpClient, HttpErrorResponse } from "@angular/common/http";
import { AuthService } from "../../security/auth.service";
import { AlbumsResponse } from "../../models/Album";
import {
  pluck,
  map,
  tap,
  catchError,
  startWith,
  distinctUntilChanged,
  mergeAll,
  concatAll,
  switchAll,
  switchMap,
  filter
} from "rxjs/operators";
import {
  EMPTY,
  throwError,
  of,
  concat,
  Subject,
  ReplaySubject,
  BehaviorSubject,
  AsyncSubject
} from "rxjs";

@Injectable({
  providedIn: "root"
})
export class MusicSearchService {
  constructor(
    @Inject(API_SEARCH_URL) private search_url: string,
    private http: HttpClient
  ) {
    (window as any).subject = this.albumChanges;

    this.queryChanges
      .pipe(
        map(query => ({
          type: "album",
          q: query
        })),
        // mergeMap(params =>
        // concatMap(params =>
        switchMap(params =>
          this.http
            .get<AlbumsResponse>(this.search_url, { params })
            .pipe(catchError(err => []))
        ),
        // mergeAll(),
        // concatAll(),
        // switchAll()
        map(resp => resp.albums.items)
      )
      .subscribe(albums => this.albumChanges.next(albums));
  }

  private results: Album[] = [
    {
      id: "123",
      name: "Test",
      images: [
        {
          url: "https://www.placecage.com/gif/200/200"
        }
      ]
    },
    {
      id: "123",
      name: "Test",
      images: [
        {
          url: "https://www.placecage.com/gif/300/300"
        }
      ]
    },
    {
      id: "123",
      name: "Test",
      images: [
        {
          url: "https://www.placecage.com/gif/300/300"
        }
      ]
    },
    {
      id: "123",
      name: "Test",
      images: [
        {
          url: "https://www.placecage.com/gif/300/300"
        }
      ]
    },
    {
      id: "123",
      name: "Test",
      images: [
        {
          url: "https://www.placecage.com/gif/300/300"
        }
      ]
    },
    {
      id: "123",
      name: "Test",
      images: [
        {
          url: "https://www.placecage.com/gif/300/300"
        }
      ]
    },
    {
      id: "123",
      name: "Test",
      images: [
        {
          url: "https://www.placecage.com/gif/300/300"
        }
      ]
    },
    {
      id: "123",
      name: "Test",
      images: [
        {
          url: "https://www.placecage.com/gif/300/300"
        }
      ]
    },
    {
      id: "123",
      name: "Test",
      images: [
        {
          url: "https://www.placecage.com/gif/300/300"
        }
      ]
    },
    {
      id: "123",
      name: "Test",
      images: [
        {
          url: "https://www.placecage.com/gif/400/400"
        }
      ]
    },
    {
      id: "123",
      name: "Test",
      images: [
        {
          url: "https://www.placecage.com/gif/500/500"
        }
      ]
    }
  ];

  private queryChanges = new BehaviorSubject("");
  query$ = this.queryChanges.asObservable();

  albumChanges = new BehaviorSubject<Album[]>([]);

  /* Commands */
  search(query: any) {
    this.queryChanges.next(query);
  }

  /* Queries */
  getAlbums() {
    return this.albumChanges.asObservable();
  }

  getQuery() {
    return this.query$;
  }

  wsysko = new BehaviorSubject({
    query: "",
    results: [] as Album[]
  });

  tylkoAlbumy = this.wsysko.pipe(
    map(w => w.results),
    distinctUntilChanged()
  );
  tylkoQuery = this.wsysko.pipe(
    map(w => w.query),
    distinctUntilChanged()
  );
}

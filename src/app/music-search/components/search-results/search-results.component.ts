import { Component, OnInit, Input, ChangeDetectionStrategy } from "@angular/core";
import { Album } from 'src/app/models/Album';

@Component({
  selector: "app-search-results",
  templateUrl: "./search-results.component.html",
  styleUrls: ["./search-results.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchResultsComponent implements OnInit {

  // random(){
  //   return Math.random()
  // }
  
  @Input()
  results: Album[];

  constructor() {}

  ngOnInit() {}
}

import { async, ComponentFixture, TestBed } from "@angular/core/testing";
// https://pics.me.me/dogberts-tech-support-hey-it-hello-shut-up-work-and-2897904.png

import { PlaylistDetailsComponent } from "./playlist-details.component";
import { By } from "@angular/platform-browser";

describe("PlaylistDetailsComponent", () => {
  let component: PlaylistDetailsComponent;
  let fixture: ComponentFixture<PlaylistDetailsComponent>;

  beforeEach(async(() => {
    return TestBed.configureTestingModule({
      declarations: [PlaylistDetailsComponent],
      imports: [],
      providers: []
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaylistDetailsComponent);
    component = fixture.componentInstance;
    component.playlist = {
      id: 123,
      name: "Angular Hits",
      favorite: true,
      color: "#ff00ff"
    };
    fixture.detectChanges();
  });

  it("should be created", () => {
    expect(component).toBeDefined();
  });

  it("should render", () => {
    // expect(
    //   fixture.debugElement.query(By.css("dd:nth-child(2)")).nativeElement
    //     .innerText
    // ).toEqual("Angular Hits");
    expect(findByCSS('dd:nth-child(2)').nativeElement.innerText).toEqual("Angular Hits")
    expect(findByCSS('dd:nth-child(4)').nativeElement.innerText).toEqual("Yes")
    expect(findByCSS('dd:nth-child(6)').nativeElement.innerText).toEqual("#ff00ff")
  });

  it('should emit event when Edit button is clicked',()=>{
    const button = findByCSS('input[type=button]')

    const editSpy = spyOn(component,'onEdit').and.callThrough()
    const eventSpy = jasmine.createSpy('EventSpy')
    
    component.edit.subscribe(eventSpy)
    
    // button.nativeElement.dispatchEvent( new MouseEvent('click') )
    button.triggerEventHandler('click',{  })

    expect(editSpy).toHaveBeenCalled()
    expect(eventSpy).toHaveBeenCalled()
  })
  
  function findByCSS(css){
    return fixture.debugElement.query(By.css(css))
  }
});

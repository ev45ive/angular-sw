import {
  Component,
  OnInit,
  ViewEncapsulation,
  Input,
  EventEmitter,
  Output
} from "@angular/core";
import { Playlist } from "src/app/models/Playlist";
import { NgForOf, NgForOfContext } from "@angular/common";

NgForOfContext;

@Component({
  selector: "app-items-list",
  templateUrl: "./items-list.component.html",
  styleUrls: ["./items-list.component.scss"],
  encapsulation: ViewEncapsulation.Emulated
  // inputs:[
  //   'playlists:items',
  // ]
})
export class ItemsListComponent implements OnInit {
  
  hover: Playlist;

  @Input("items")
  playlists: Playlist[] = [];

  @Input()
  selected: Playlist = this.playlists[0];

  @Output()
  selectedChange = new EventEmitter<Playlist>();

  select(playlist: Playlist) {
    // this.selected = playlist;
    this.selectedChange.emit(playlist);
  }

  trackFn(p: Playlist, index: number) {
    return p.id;
  }

  constructor() {}

  ngOnInit() {}
}

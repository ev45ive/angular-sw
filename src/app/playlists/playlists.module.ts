import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { PlaylistsRoutingModule } from "./playlists-routing.module";
import { PlaylistsViewComponent } from "./views/playlists-view/playlists-view.component";
import { ItemsListComponent } from "./components/items-list/items-list.component";
import { ListItemComponent } from "./components/list-item/list-item.component";
import { PlaylistDetailsComponent } from "./components/playlist-details/playlist-details.component";
import { PlaylistFormComponent } from "./components/playlist-form/playlist-form.component";

import { FormsModule } from "@angular/forms";
import { SharedModule } from "../shared/shared.module";

@NgModule({
  declarations: [
    PlaylistsViewComponent,
    ItemsListComponent,
    ListItemComponent,
    PlaylistDetailsComponent,
    PlaylistFormComponent
  ],
  imports: [CommonModule, PlaylistsRoutingModule, FormsModule, SharedModule],
  exports: [PlaylistsViewComponent],

  entryComponents: [PlaylistsViewComponent]
})
export class PlaylistsModule {}

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PlaylistsModule } from './playlists/playlists.module';
import { SharedModule } from './shared/shared.module';
import { SecurityModule } from './security/security.module';
import { API_SEARCH_URL } from './music-search/services/tokens';
import { environment } from 'src/environments/environment';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    SharedModule,
    SecurityModule,
    PlaylistsModule,
    // MusicSearchModule,
    HttpClientModule, 
    AppRoutingModule,
  ],
  // entryComponents:[AppComponent],
  providers: [
    {
      provide: API_SEARCH_URL,
      useValue: environment.api_url
    }
  ],
  // bootstrap: [HeaderComponent, AppComponent, FooterComponent]
  bootstrap: [AppComponent]
})
export class AppModule {

  // constructor(private app:ApplicationRef){}

  // ngDoBootstrap(){
  //   this.app.bootstrap(AppComponent,'app-root')
  //   this.app.bootstrap(AppComponent,'app-root2')
  //   this.app.bootstrap(AppComponent,'app-root3')
  // }
}
